FROM anapsix/alpine-java:8_server-jre
MAINTAINER Michal Buczko "michal.buczko@gmail.com"

ENV GOSU_VERSION 1.10

RUN apk --no-cache --update upgrade
RUN apk add --update ca-certificates openssl && apk add dpkg
RUN dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')" && \
    wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch" && \
    wget https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/tar/elasticsearch/2.4.4/elasticsearch-2.4.4.tar.gz
RUN tar xzvf elasticsearch-2.4.4.tar.gz && mv elasticsearch-2.4.4 /opt/elastic && rm elasticsearch-2.4.4.tar.gz && \
    chmod +x /usr/local/bin/gosu && \
    gosu nobody true && \
    apk del dpkg

RUN addgroup -S elastic && adduser -S -g elastic elastic

ADD docker-entrypoint.sh /
ADD elasticsearch.yml /opt/elastic/config
ADD elasticsearch-consul-discovery-2.0-SNAPSHOT.zip /
# RUN /opt/elastic/bin/plugin install file:///elasticsearch-consul-discovery-2.0-SNAPSHOT.zip
RUN rm /elasticsearch-consul-discovery-2.0-SNAPSHOT.zip
RUN mkdir /opt/elastic/data /opt/elastic/logs /opt/elastic/config/scripts /opt/elastic/plugins /opt/elastic/data/elastica
RUN chown -R elastic:elastic /opt/elastic/data

WORKDIR /opt/elastic
EXPOSE 9200 9300

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["bin/elasticsearch"]
